#!/usr/bin/env python

from argparse import ArgumentParser
from FreenectPlaybackWrapper.PlaybackWrapper import FreenectPlaybackWrapper

import cv2 
import time
import numpy as np

def main():
    i1 = "ExampleVideo"
    i2 = "Set1\Set1"
    i3 = "Set2\Set2"
    parser = ArgumentParser(description="OpenCV Demo for Kinect Coursework")
    parser.add_argument("videofolder", help="Folder containing Kinect video. Folder must contain INDEX.txt.",
                        default=i2, nargs="?")
    parser.add_argument("--no-realtime", action="store_true", default=False)

    args = parser.parse_args()
    player = FreenectPlaybackWrapper(args.videofolder, not args.no_realtime);
    test = True
    

    temp = cv2.imread('sample.jpg',0)
    cnt = 0
    for status, rgb, depth in FreenectPlaybackWrapper(args.videofolder, not args.no_realtime):

        # If we have an updated RGB image, then display
        #ret,testDepth = cv2.threshold(depth,100,255,cv2.THRESH_BINARY_INV)
        ret, testDepth = cv2.threshold(depth, 80, 255, cv2.THRESH_TOZERO_INV)
        #contours, hierarchy = cv2.findContours(testDepth, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        edges = cv2.Canny(testDepth,10, 100)
        contours, hierarchy = cv2.findContours(edges, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
       # print("cont num {}".format(str(len(contours))))
        if len(contours) > 1:
            nn = contours[len(contours)-1]
            v = len(contours) -1

            cv2.drawContours(rgb, contours, -1, (0, 255, 0), 6)

        #cv2.drawContours(rgb, contours, -1, (0, 230, 255), 6)

        if status.updated_rgb:
            cv2.imshow("RGB", rgb)
        # If we have an updated Depth image, then display
        if status.updated_depth:
            cv2.imshow("Depth", testDepth)
           # cv2.imshow("Depth2", depth)


        # Check for Keyboard input.
        key = cv2.waitKey(10)

        # Break out of the program if ESC is pressed (OpenCV KeyCode 27 is ESC key)
        if key == 27:
            break
        #cnt = cnt + 1
    return 0

if __name__ == "__main__":
    exit(main())


#if cnt == 120:
            #cv2.imshow("sampl", depth)
# time.sleep(1)
# cnt = cnt + 1

# if cnt > 120:

# gray=cv2.cvtColor(depth,cv2.COLOR_BGR2GRAY)
# result=cv2.matchTemplate(depth,temp,cv2.TM_CCOEFF)

# sin_val, max_val, min_loc, max_loc=cv2.minMaxLoc(result)
# top_left=max_loc
# print ("correlation {}".format(top_left))
# increasing the size of bounding rectangle by 50 pixels
# bottom_right=(top_left[0]+100,top_left[1]+200)
# cv2.rectangle(depth, top_left, bottom_right, (0,255,0),5)

# cv2.imshow('object found',depth)