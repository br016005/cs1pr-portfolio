#!/usr/bin/env python

from argparse import ArgumentParser
from FreenectPlaybackWrapper.PlaybackWrapper import FreenectPlaybackWrapper

import cv2 
import os
import numpy as np
import mahotas
import matplotlib.pyplot as plt

def sampleRun():
    i2 = "Set1\Set1"
    parser = ArgumentParser(description="OpenCV Demo for Kinect Coursework")
    parser.add_argument("videofolder", help="Folder containing Kinect video. Folder must contain INDEX.txt.",
                        default=i2, nargs="?")
    parser.add_argument("--no-realtime", action="store_true", default=False)

    args = parser.parse_args()
    # lables for the obejects being show  in order
    labels = ["baby", "dog", "dinosaur", "coffee tin", "mug", "car", "camera", "keyboard", "koala", "blackberry",
              "diet coke bottle", "duck", "dragon", "andriod"]

    # tracking the current label
    cnt = 0
    frame = 1
    # tracking amount of frames which have occured
    timePassed = 0

    # stores whether of not an image was recently shown
    shown = False;
    for status, rgb, depth in FreenectPlaybackWrapper(args.videofolder, not args.no_realtime):

        #applies a transformation to the depth data so that it lines up with the rgb image
        rows, cols, dim = depth.shape
        M = np.float32([[1, 0, -40],
                        [0, 1, 30],
                        [0, 0, 1]])
        #the tranformed depth image
        translated_img = cv2.warpPerspective(depth, M, (cols, rows))

        #applies thresholding to the depth data to only obtain the relavent informaiton
        ret, testDepth = cv2.threshold(translated_img, 80, 255, cv2.THRESH_TOZERO_INV)

        #finds the edges anc contorus of the current depth data
        edges = cv2.Canny(testDepth,10, 100)
        contours, hierarchy = cv2.findContours(edges, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

        #when there is more than 1 contour an object is most likely on screen so this shows the object
        if len(contours) > 1:
            cv2.drawContours(rgb, contours, -1, (0, 255, 0), 6)
            cv2.putText(rgb, labels[cnt], (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2, cv2.LINE_AA)

            #rect = cv2.boundingRect(contours[0]);
            #point1 = cv2.
            #point2 = cv2.Point(rect.x + rect.width, rect.y + rect.height);
            #cv2.rectangle(testDepth, point1, point2, (255,255,255), 2, cv2.LINE_AA, 0);

            if frame < 10:
                cv2.imwrite("samples\{}\{}{}{}sample.png".format(labels[cnt], 0,0,frame), testDepth, (50, 50))
            elif frame < 100:
                cv2.imwrite("samples\{}\{}{}sample.png".format(labels[cnt], 0, frame), testDepth, (50, 50))
            else:
                cv2.imwrite("samples\{}\{}sample.png".format(labels[cnt], frame), testDepth, (50, 50))
            frame = frame + 1
            #this is set to the running values
            shown = True
            timePassed = 0
        #if an image had been shown and 30 frames have passed it will move to the next item in the labels
        elif shown == True and timePassed > 30:
            cnt = cnt + 1
            shown = False
            frame = 0
        #counts the amount of time passed.
        else:
            timePassed = timePassed + 1


        if status.updated_rgb:
            cv2.imshow("RGB", rgb)
        # If we have an updated Depth image, then display
        if status.updated_depth:
            cv2.imshow("Depth", testDepth)

        # Check for Keyboard input.
        key = cv2.waitKey(10)

        # Break out of the program if ESC is pressed (OpenCV KeyCode 27 is ESC key)
        if key == 27:
            break

    return 0

def testRun():
    i3 = "Set2\Set2"
    parser = ArgumentParser(description="OpenCV Demo for Kinect Coursework")
    parser.add_argument("videofolder", help="Folder containing Kinect video. Folder must contain INDEX.txt.",
                        default=i3, nargs="?")
    parser.add_argument("--no-realtime", action="store_true", default=False)

    args = parser.parse_args()

    for status, rgb, depth in FreenectPlaybackWrapper(args.videofolder, not args.no_realtime):

        # If we have an updated RGB image, then display
        if status.updated_rgb:
            cv2.imshow("RGB", rgb)

        # If we have an updated Depth image, then display
        if status.updated_depth:
            cv2.imshow("Depth", depth)

        # Check for Keyboard input.
        key = cv2.waitKey(10)

        # Break out of the program if ESC is pressed (OpenCV KeyCode 27 is ESC key)
        if key == 27:
            break

    return 0

def fd_hu_moments(image):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    feature = cv2.HuMoments(cv2.moments(image)).flatten()
    return feature


# In[5]:


# feature-descriptor -2 Haralick Texture

def fd_haralick(image):
    # conver the image to grayscale
    gray = cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    # Ccompute the haralick texture fetature ve tor
    haralic = mahotas.features.haralick(gray).mean(axis=0)
    return haralic


# In[6]:


# feature-description -3 Color Histogram

def fd_histogram(image, mask=None):
    # conver the image to HSV colors-space
    image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    #COPUTE THE COLOR HISTPGRAM
    hist  = cv2.calcHist([image],[0,1,2],None,[8,8,8], [0, 256, 0, 256, 0, 256])
    # normalize the histogram
    cv2.normalize(hist,hist)
    # return the histog....
    return hist.flatten()

def classification():
    dir = os.path.join("samples", "dog")
    global_features = []
    # get the current training label

    print("test {}".format(dir))
    k = 1
    lst = os.listdir(dir)
    lst.sort()
    for file in lst:

        file = dir + "/" + os.fsdecode(file)
        print("2 {}".format(file))
        # read the image and resize it to a fixed-size
        image = cv2.imread(file)
        if image is not None:
            fv_hu_moments = fd_hu_moments(image)
            fv_haralick = fd_haralick(image)
            fv_histogram = fd_histogram(image)

        global_feature = np.hstack([fv_histogram, fv_haralick, fv_hu_moments])
        prediction = clf.predict(global_feature.reshape(1, -1))[0]
        global_features.append(global_feature)
        cv2.putText(image, "dog", (20, 30), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0, 255, 255), 3)

        # display the output image
        plt.imshow(cv2.cvtColor(image, cv2.COLOR_BGR2RGB))
        plt.show()

        # print("image not loaded")
        #cv2.imshow("test", image)
        #key = cv2.waitKey(10)




def main():
    i1 = "ExampleVideo"
    i2 = "Set1\Set1"
    i3 = "Set2\Set2"
    #sampleRun();
    #testRun();
    classification()







if __name__ == "__main__":
    exit(main())


#if cnt == 120:
            #cv2.imshow("sampl", depth)
# time.sleep(1)
# cnt = cnt + 1

# if cnt > 120:

# gray=cv2.cvtColor(depth,cv2.COLOR_BGR2GRAY)
# result=cv2.matchTemplate(depth,temp,cv2.TM_CCOEFF)

# sin_val, max_val, min_loc, max_loc=cv2.minMaxLoc(result)
# top_left=max_loc
# print ("correlation {}".format(top_left))
# increasing the size of bounding rectangle by 50 pixels
# bottom_right=(top_left[0]+100,top_left[1]+200)
# cv2.rectangle(depth, top_left, bottom_right, (0,255,0),5)
 # print("cont num {}".format(str(len(contours))))
 #ret,testDepth = cv2.threshold(depth,100,255,cv2.THRESH_BINARY_INV)
 #contours, hierarchy = cv2.findContours(testDepth, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
 # print("cont num {}".format(str(len(contours))))
# cv2.imshow('object found',depth)


 #all contours
           # allCont = contours[len(contours)-1]
            #number of conrours

          #  numCont = len(contours) -1
           # mask = np.zeros_like(rgb)
           # cv2.drawContours(mask, contours, -1, (0, 255, 0), 6)
           # cv2.imwrite('opencv_draw_mask.jpg', mask)

           # boundRect = [None] * len(contours)
           # contours_poly = [None] * len(contours)
            #for i, c in enumerate(contours):
             #   contours_poly[i] = cv2.approxPolyDP(c, 3, True)
             #   boundRect[i] = cv2.boundingRect(contours_poly[i])
           # for i in range(len(contours)):
               # cv2.rectangle(rgb, (int(boundRect[i][0]), int(boundRect[i][1])), \
                     #    (int(boundRect[i][0] + boundRect[i][2]), int(boundRect[i][1] + boundRect[i][3])), (0,255,0), 2)
            #draws the all the contours onto the rgb image with the correct label